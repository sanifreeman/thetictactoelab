public class Board {
  
    private Square[][] tictactoeBoard;
    
    
    public Board(){
      tictactoeBoard = new Square[3][3];
      for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
           tictactoeBoard[i][j]=Square.BLANK;
        }
      }
    }
    
    //Override
    public String toString() {
      String boardRep = "";
      for (int i = 0; i < 3; i++) {
          for (int j = 0; j < 3; j++) {
              boardRep += tictactoeBoard[i][j].toString() + " ";
          }
          boardRep += "\n";
      }
      return boardRep;
     }
   
    public boolean placeToken(int row, int column, Square playerToken) {
      
      if( row>2 || row<0 || column>2 || column<0){
        return false;
      }
      
      if(tictactoeBoard[row][column]==Square.BLANK ){
        
        tictactoeBoard[row][column]=playerToken;
        return true;
      }
      
      return false;
    }
        
    
    public boolean checkIfFull(){
      
      for(int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          if (tictactoeBoard[i][j] == Square.BLANK){
            return false;
          }
        }
      }
      return true;
    }
    
    private boolean checkIfWinningHorizontal(Square playerToken){
      
      for(int i=0; i<3;i++){
        if(tictactoeBoard[i][0]==playerToken && tictactoeBoard[i][1]==playerToken && tictactoeBoard[i][2]==playerToken) {
          return true;
        }
      }
      return false;
    }
    
     private boolean checkIfWinningVetical(Square playerToken){
      
      for(int j=0; j<3;j++){
        if(tictactoeBoard[0][j]==playerToken && tictactoeBoard[1][j]==playerToken && tictactoeBoard[2][j]==playerToken) {
          return true;
        }
      }
      return false;
    }
     
     private boolean checkIfWinningDiagonal( Square playerToken){
       if (tictactoeBoard[0][0] == playerToken && tictactoeBoard[1][1]== playerToken && tictactoeBoard[2][2]== playerToken){
         return true;
       }
       
        if (tictactoeBoard[0][2] == playerToken && tictactoeBoard[1][1]== playerToken && tictactoeBoard[2][0]== playerToken){
         return true;
        }
        return false;
     }
       
  
     public boolean checkIfWinning (Square playerToken){
       if (checkIfWinningHorizontal(playerToken) || checkIfWinningVetical(playerToken)|| checkIfWinningDiagonal(playerToken)){
         return true;
       }
       return false;
     }
  
    }
  