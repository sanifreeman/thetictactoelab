import java.util.Scanner;

public class TicTacToeGame {
  
  public static void main(String [] args){
    
    Board board = new Board(); 
    
    System.out.println("Welcome To Hasani's tictactoe !!!");
    System.out.println("Player 1 token: X");
    System.out.println("Player 2 token: O");
    
    boolean gameOver=false;
    int player=1;
    Square playerToken=Square.X;
    
    Scanner keyboard= new Scanner(System.in);
    
    while(!gameOver){
      
      System.out.println(board);
        
        if(player==1){
        playerToken=Square.X;
      } else {
        playerToken=Square.O;
      }
    
    boolean positionInput=false;
    
    while(!positionInput){
      
    System.out.println("Player " + player +": it's your turn, whats your move put the row number press enter then enter column number");
    
    int row = (keyboard.nextInt())-1;
    int column= (keyboard.nextInt())-1;
    
    positionInput=board.placeToken(row, column, playerToken);
    
       if (!positionInput){
         System.out.println("please re-enter inputs");
       }
    }   
    
    if(board.checkIfFull()){
       System.out.println(board); 
      System.out.println("It's a tie ! :(");
      gameOver=true;
      
    } else if(board.checkIfWinning(playerToken)){
       System.out.println(board); 
      System.out.println("Player "+ player+" is the winner!");
      gameOver=true;
      
    } else { 
      
      player++;
      if(player>2){
        player=1;
      }  
    }
    
   }
  }
}
    
    
    
    
    
    
    